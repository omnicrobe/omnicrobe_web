#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from flask import g, jsonify
import psycopg2
from psycopg2 import pool
import re
from omnicrobe_web.config import *

# Get Database connection
def get_db(app):
    if 'db' not in g:
        g.db = app.config['postgreSQL_pool'].getconn()
    return g.db

# Check source format
def check_source(s):
    sources = ['PubMed', 'GenBank', 'DSMZ', 'CIRM-BIA', 'CIRM-Levures', 'CIRM-CFBP']
    final = ''
    for source in sources:
        pattern = re.compile(source, re.IGNORECASE)
        final = pattern.sub(source, s)
        if final != s or final in sources:
            return final

# Available sources
sources = {
            "CIRM-BIA".lower() : "https://collection-cirmbia.fr/page/Display_souches/",
            "CIRM-Levures".lower() : "",
            "CIRM-CFBP".lower() : "",
            "DSMZ".lower() : "https://bacdive.dsmz.de/strain/",
            "GenBank".lower() : "https://www.ncbi.nlm.nih.gov/nuccore/",
            # "PubMed".lower() : "https://pubmed.ncbi.nlm.nih.gov/"
            "PubMed".lower() : ["https://pubmed.ncbi.nlm.nih.gov/", "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=pubmed&id=PMID&rettype=xml"]
          }

# Get version: /get/version
def get_version(app, conn):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT omnicrobe, taxonomy, ontobiotope, pubmed, dsmz, genbank, cirmbia, cirmlevures, cirmcfbp \
                            FROM v_source")
        except Exception as err:
            print_psycopg2_exception(err)
        columns = ('omnicrobe', 'taxonomy', 'ontobiotope', 'pubmed', 'dsmz', 'genbank', 'cirmbia', 'cirmlevures', 'cirmcfbp')
        return dict(zip(columns, cursor.fetchall()[0]))
        cursor.close()

# Get taxon: /get/taxon
def get_taxon(app, conn, id):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT taxonid, name, path, qps \
                            FROM taxon \
                            WHERE lower(taxonid) = '" + id.lower() +"'")
        except Exception as err:
            print_psycopg2_exception(err)
        results = dict()
        for row in cursor.fetchall():
            results['id'] = row[0]
            results['name'] = row[1]
            results['path'] = []
            for path in row[2].split(","):
                results['path'].append(path)
            results['qps'] = row[3]
            if "ncbi" in row[0]:
                results['url'] = {}
                results['url']['entrez-api'] = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id="+row[0].replace("ncbi:", "")+"&rettype=xml"
                results['url']['ncbi-web'] = "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[0].replace("ncbi:", "")
                # results['url'] = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id="+row[0].replace("ncbi:", "")+"&rettype=xml"
            else:
                results['url'] = {}
                results['url']['dsmz-web'] = "https://bacdive.dsmz.de/strain/"+row[0].replace("bd:", "")
                # results['url'] = "https://bacdive.dsmz.de/strain/"+row[0].replace("bd:", "")
        cursor.close()
        return results
    
# Get taxons: /get/taxons
def get_taxons(app, conn, name):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT taxonid, name, path, qps \
                            FROM taxon \
                            WHERE lower(name) ~ '"+ name.lower() +"'")
        except Exception as err:
            print_psycopg2_exception(err)
        results = dict()
        results['name'] = name
        taxons = []
        for row in cursor.fetchall():
            taxon = dict()
            taxon['id'] = row[0]
            taxon['name'] = row[1]
            taxon['path'] = []
            for path in row[2].split(","):
                taxon['path'].append(path)
            taxon['qps'] = row[3]
            # if "ncbi" in row[0]:
            #     taxon['url'] = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id="+row[0].replace("ncbi:", "")+"&rettype=xml"
            # else:
            #     taxon['url'] = "https://bacdive.dsmz.de/strain/"+row[0].replace("bd:", "")
            if "ncbi" in row[0]:
                taxon['url'] = {}
                taxon['url']['entrez-api'] = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id="+row[0].replace("ncbi:", "")+"&rettype=xml"
                taxon['url']['ncbi-web'] = "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id="+row[0].replace("ncbi:", "")
                # results['url'] = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=taxonomy&id="+row[0].replace("ncbi:", "")+"&rettype=xml"
            else:
                taxon['url'] = {}
                taxon['url']['dsmz-web'] = "https://bacdive.dsmz.de/strain/"+row[0].replace("bd:", "")
                # results['url'] = "https://bacdive.dsmz.de/strain/"+row[0].replace("bd:", "")
            taxons.append(taxon)
        results['taxons'] = taxons
        cursor.close()
        return results

# Get OntoBiotope: /get/ontobiotope
def get_ontobiotope(app, conn, id):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute("SELECT identifier, name, path, type, synonym \
                            FROM element \
                            WHERE lower(identifier) = '" + id.lower() +"'")
        except Exception as err:
            print_psycopg2_exception(err)
        results = dict()
        for row in cursor.fetchall():
            results['id'] = row[0]
            results['name'] = row[1]
            results['path'] = []
            for path in row[2].split(","):
                results['path'].append(path)
            results['type'] = row[3]
            results['synonyms'] = []
            if row[4] != None:
                for synonym in row[4].split(","):
                    results['synonyms'].append(synonym)
        cursor.close()
        return results

# Get Source text: /get/doc
def get_doc(app, conn, source, id):
    if source.lower() in sources.keys():
        conn = get_db(app)
        if conn != None:
            cursor = conn.cursor()
            ss = check_source(source)
            try:
                query = "SELECT id_source FROM relation \
                         WHERE id_source like '%" + str(id) +"%' \
                         AND source = '" + ss + "'"
                cursor.execute(query)
                print(query)
            except Exception as err:
                print_psycopg2_exception(err)
            results = dict()
            if cursor.fetchall() != []:
                if source.lower() == 'pubmed':
                    results['url'] = {}
                    results['url']['pubmed-web'] = sources[source.lower()][0] + id
                    results['url']['entrez-api'] = sources[source.lower()][1].replace("PMID", id)
                else:
                    results['url'] = {}
                    results['url'][source.lower()+'-web'] = sources[source.lower()] + id
            cursor.close()
    else:
        results = "no"
    return results

# Search taxon: /search/taxon
def search_taxon(app, conn, root, qps, name):
    query = "SELECT taxonid FROM taxon WHERE "
    if qps in ("yes", "true"):
        query += "qps = 'yes' AND "
    if name != None:
        query += "lower(name) ~ '" + name.lower() + "' AND "
    if root != None:
        query += "(lower(taxonid) = '" + root.lower() + "' or lower(path) like '%/" + root.lower() + "/%')"
    query = re.sub('AND $', '', query)
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            results.append({"id" : row[0]})
        cursor.close()
        return results

# Search OntoBiotope: /search/ontobiotope
def search_ontobiotope(app, conn, root, type, name):
    query = "SELECT identifier FROM element WHERE "
    if type != None:
        query += "type = '" + type + "' AND "
    if name != None:
        query += "(lower(name) ~ '" + name.lower() + "' or lower(synonym)  ~ '" + name.lower() + "') AND "
    if root != None:
        query += "(lower(identifier) = '" + root.lower() + "' or path like '%/" + root.lower() + "/%')"
    query = re.sub('AND $', '', query)
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            results.append({"id" : row[0]})
        cursor.close()
        return results

# Search relations: /search/relations
def search_relations(app, conn, source, taxonids, qps, ontobiotopeids, types, docid):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []

        if taxonids == [] and ontobiotopeids != []:
            taxonid = None
            for ontobiotopeid in ontobiotopeids:
                results += create_sql_relation(source, qps, types, taxonid, ontobiotopeid, docid, cursor)
        elif taxonids != [] and ontobiotopeids == []:
            ontobiotopeid = None
            for taxonid in taxonids:
                results += create_sql_relation(source, qps, types, taxonid, ontobiotopeid, docid, cursor)
        else:
            for taxonid in taxonids:
                for ontobiotopeid in ontobiotopeids:
                    results += create_sql_relation(source, qps, types, taxonid, ontobiotopeid, docid, cursor)

        cursor.close()
        return results

# Create SQL query for Search relations
def create_sql_relation(source, qps, types, taxonid, ontobiotopeid, docid, cursor):
    check = 'yes'
    if taxonid == None:
        taxroot = "null"
    else:
        taxroot = taxonid
    if ontobiotopeid == None:
        obtroot = "ALL"
    else:
        obtroot = ontobiotopeid
    results = []
    query = "SELECT r.form_taxon, r.form_element, r.id_source, \
r.type, r.source, e.identifier, t.taxonid, e.name, t.name"
    query += " FROM relation r, taxon t, element e"
    query += " WHERE r.id_element = e.id AND t.id = r.id_taxon"
    if source != []:
        query += " AND ("
        for s in source:
            ss = check_source(s)
            query += "r.source = '" + ss + "' OR "
        query += ")"
        query = re.sub(" OR \)", ")", query)
    if docid != None:
        query += " AND id_source like '%"+ docid +"%'"
    if qps in ("yes", "true"):
        query += " AND t.qps = 'yes'"
    if types != []:
        query += " AND ("
        for type in types:
            query += "r.type = '" + type + "' OR "
        query += ")"
        query = re.sub(' OR \)', ')', query)
    if taxonid != None:
        query += " AND r.id_taxon in (SELECT id FROM taxon WHERE lower(taxonid) = '"+taxonid.lower()+"' OR path like '%/"+taxonid.lower()+"/%')"
    if ontobiotopeid != None:
        query += " AND r.id_element in (SELECT id FROM element WHERE lower(identifier) = '"+ontobiotopeid.lower()+"' OR path like '%/"+ontobiotopeid.lower()+"/%')"
    # Only if taxID and OBTID exist
    if check == 'yes':
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        for row in cursor.fetchall():
            elements = {}
            t_obj = {}
            e_obj = {}
            # taxon_forms
            str_taxforms = clean_form(row[0])
            elements['taxon_forms'] = []
            for taxform in str_taxforms.split(", "):
                if taxform not in elements['taxon_forms']:
                    elements['taxon_forms'].append(taxform)
            # obt_forms
            str_obtforms = clean_form(row[1])
            elements['obt_forms'] = []
            sep = ", "
            if row[4] in ('DSMZ', 'GenBank'):
                sep = "|"
            #for obtform in str_obtforms.split(", "):
            for obtform in str_obtforms.split(sep):
                if obtform not in elements['obt_forms']:
                    elements['obt_forms'].append(obtform)
            # docs
            elements['docs'] = []
            for doc in row[2].split(", "):
                elements['docs'].append(doc)
            # type, source, obtid, taxid
            elements['type'] = row[3]
            elements['source'] = row[4]
            elements['obtid'] = row[5]
            e_obj['obtid'] = row[5]
            e_obj['name'] = row[7]
            elements['taxid'] = row[6]
            t_obj['taxid'] = row[6]
            t_obj['name'] = row[8]
            elements['taxroot'] = taxroot
            elements['obtroot'] = obtroot
            # ajout easy16S
            elements['taxon_object'] = t_obj
            elements['obt_object'] = e_obj
            results.append(elements)
    return results

# Search join-relations by Taxon: /search/join-relations
def search_join_relations_by_taxon(app, conn, source, qps, lefttype, leftroots, righttype, rightroots, joinroot, jointype):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []
        for leftroot in leftroots:
            for rightroot in rightroots:
                if leftroot == rightroot:
                    continue
                query  = "SELECT le.identifier AS leftId, re.identifier AS rightId, je.taxonid AS joinId, lr.source AS leftSource, rr.source AS rightSource, lr.id_source AS leftDocs, rr.id_source AS rightDocs "
                query += "FROM element le, taxon je, element re, relation lr, relation rr "
                query += "WHERE lr.id_taxon = je.id "
                query += "AND lr.id_element = le.id "
                query += "AND rr.id_taxon = je.id "
                query += "AND rr.id_element = re.id "
                query += "AND lr.type = '" + lefttype + "' "
                query += "AND rr.type = '" + righttype + "' "
                query += "AND le.id != re.id "
                query += "AND (lower(le.path) like '%/" + leftroot.lower() + "/%' OR lower(le.identifier) = '"+ leftroot.lower() +"') "
                query += "AND (lower(re.path) like '%/" + rightroot.lower() + "/%' OR lower(re.identifier) = '"+ rightroot.lower() +"') "
                if joinroot != None:
                    query += "AND (lower(je.path) like '%/" + joinroot.lower() +"/%' OR lower(je.taxonid) = '"+ joinroot.lower() +"') "
                if qps in ("yes", "true"):
                    query += "AND je.qps = 'yes' "
                if source != []:
                    query = add_sources(query, source)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                for row in cursor.fetchall():
                    elements = {}
                    elements['leftId'] = row[0]
                    elements['rightId'] = row[1]
                    elements['joinId'] = row[2]
                    elements['leftSource'] = row[3]
                    elements['rightSource'] = row[4]
                    elements['leftDocs'] = []
                    for ldoc in row[5].split(", "):
                        elements['leftDocs'].append(ldoc)
                    elements['rightDocs'] = []
                    for rdoc in row[6].split(", "):
                        elements['rightDocs'].append(rdoc)
                    elements['leftType'] = lefttype
                    elements['joinType'] = jointype
                    elements['rightType'] = righttype
                    elements['leftRoot'] = leftroot
                    elements['rightRoot'] = rightroot
                    results.append(elements)
        cursor.close()
        return results
# Search join-relations by Taxon aggregate
def search_join_relations_by_taxon_aggregate(app, conn, source, qps, lefttype, leftroots, righttype, rightroots, joinroot, jointype):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []
        for leftroot in leftroots:
            for rightroot in rightroots:
                if leftroot == rightroot:
                    continue
                query  = "SELECT le.identifier AS leftId, re.identifier AS rightId, je.taxonid AS joinId, lr.source AS leftSource, rr.source AS rightSource, lr.id_source AS leftDocs, rr.id_source AS rightDocs "
                query += "FROM element le, taxon je, element re, relation lr, relation rr "
                query += "WHERE lr.id_taxon = je.id "
                query += "AND lr.id_element = le.id "
                query += "AND rr.id_taxon = je.id "
                query += "AND rr.id_element = re.id "
                query += "AND lr.type = '" + lefttype + "' "
                query += "AND rr.type = '" + righttype + "' "
                query += "AND le.id != re.id "
                query += "AND (lower(le.path) like '%/" + leftroot.lower() + "/%' OR lower(le.identifier) = '"+ leftroot.lower() +"') "
                query += "AND (lower(re.path) like '%/" + rightroot.lower() + "/%' OR lower(re.identifier) = '"+ rightroot.lower() +"') "
                if joinroot != None:
                    query += "AND (lower(je.path) like '%/" + joinroot.lower() +"/%' OR lower(je.taxonid) = '"+ joinroot.lower() +"') "
                if qps in ("yes", "true"):
                    query += "AND je.qps = 'yes' "
                if source != []:
                    query = add_sources(query, source)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                allLeftIds = {}
                allLeftDocs = {}
                allRightIds = {}
                allRightDocs = {}
                allJoinIds = {}
                elements = {}
                for row in cursor.fetchall():
                    allLeftIds[row[0]] = ''
                    allRightIds[row[1]] = ''
                    allJoinIds[row[2]] = ''
                    for ldoc in row[5].split(", "):
                        allLeftDocs[ldoc] = ''
                    for rdoc in row[6].split(", "):
                        allRightDocs[rdoc] = ''
                elements['leftType'] = lefttype
                elements['rightType'] = righttype
                elements['leftId'] = leftroot
                elements['rightId'] = rightroot
                elements['leftRoot'] = leftroot
                elements['rightRoot'] = rightroot
                elements['leftDocuments'] = len(allLeftDocs.keys())
                elements['leftDiversity'] = len(allLeftIds.keys())
                elements['rightDocuments'] = len(allRightDocs.keys())
                elements['rightDiversity'] = len(allRightIds.keys())
                elements['joinDiversity'] = len(allJoinIds.keys())
                results.append(elements)
        cursor.close()
        return results
# Search join-relations by OntoBiotope: /search/join-relations
def search_join_relations_by_ontobiotope(app, conn, source, qps, lefttype, leftroots, righttype, rightroots, joinroot, jointype):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []
        for leftroot in leftroots:
            for rightroot in rightroots:
                if leftroot == rightroot:
                    continue
                query  = "SELECT le.taxonid AS leftId, re.taxonid AS rightId, je.identifier AS joinId, lr.source AS leftSource, rr.source AS rightSource, lr.id_source AS leftDocs, rr.id_source AS rightDocs "
                query += "FROM taxon le, element je, taxon re, relation lr, relation rr "
                query += "WHERE lr.id_taxon = le.id "
                query += "AND lr.id_element = je.id "
                query += "AND rr.id_taxon = re.id "
                query += "AND rr.id_element = je.id "
                query += "AND lr.type = '" + jointype + "' "
                query += "AND rr.type = '" + jointype + "' "
                query += "AND le.id != re.id "
                query += "AND (lower(le.path) like '%/" + leftroot.lower() + "/%' OR lower(le.taxonid) = '"+ leftroot.lower() +"') "
                query += "AND (lower(re.path) like '%/" + rightroot.lower() + "/%' OR lower(re.taxonid) = '"+ rightroot.lower() +"') "
                if joinroot != None:
                    query += "AND (lower(je.path) like '%/" + joinroot.lower() +"/%' OR lower(je.identifier) = '"+ joinroot.lower() +"') "
                if qps in ("yes", "true"):
                    query += "AND je.qps = 'yes' "
                if source != []:
                    query = add_sources(query, source)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                for row in cursor.fetchall():
                    elements = {}
                    elements['leftId'] = row[0]
                    elements['rightId'] = row[1]
                    elements['joinId'] = row[2]
                    elements['leftSource'] = row[3]
                    elements['rightSource'] = row[4]
                    elements['leftDocs'] = []
                    for ldoc in row[5].split(", "):
                        elements['leftDocs'].append(ldoc)
                    elements['rightDocs'] = []
                    for rdoc in row[6].split(", "):
                        elements['rightDocs'].append(rdoc)
                    elements['leftType'] = lefttype
                    elements['joinType'] = jointype
                    elements['rightType'] = righttype
                    elements['leftRoot'] = leftroot
                    elements['rightRoot'] = rightroot
                    results.append(elements)
        cursor.close()
        return results
# Search join-relations by OntoBiotope aggregate
def search_join_relations_by_ontobiotope_aggregate(app, conn, source, qps, lefttype, leftroots, righttype, rightroots, joinroot, jointype):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []
        for leftroot in leftroots:
            for rightroot in rightroots:
                if leftroot == rightroot:
                    continue
                query  = "SELECT le.taxonid AS leftId, re.taxonid AS rightId, je.identifier AS joinId, lr.source AS leftSource, rr.source AS rightSource, lr.id_source AS leftDocs, rr.id_source AS rightDocs "
                query += "FROM taxon le, element je, taxon re, relation lr, relation rr "
                query += "WHERE lr.id_taxon = le.id "
                query += "AND lr.id_element = je.id "
                query += "AND rr.id_taxon = re.id "
                query += "AND rr.id_element = je.id "
                query += "AND lr.type = '" + jointype + "' "
                query += "AND rr.type = '" + jointype + "' "
                query += "AND le.id != re.id "
                query += "AND (lower(le.path) like '%/" + leftroot.lower() + "/%' OR lower(le.taxonid) = '"+ leftroot.lower() +"') "
                query += "AND (lower(re.path) like '%/" + rightroot.lower() + "/%' OR lower(re.taxonid) = '"+ rightroot.lower() +"') "
                if joinroot != None:
                    query += "AND (lower(je.path) like '%/" + joinroot.lower() +"/%' OR lower(je.identifier) = '"+ joinroot.lower() +"') "
                if qps in ("yes", "true"):
                    query += " AND je.qps = 'yes'"
                if source != []:
                    query = add_sources(query, source)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                allLeftIds = {}
                allLeftDocs = {}
                allRightIds = {}
                allRightDocs = {}
                allJoinIds = {}
                elements = {}
                for row in cursor.fetchall():
                    allLeftIds[row[0]] = ''
                    allRightIds[row[1]] = ''
                    allJoinIds[row[2]] = ''
                    for ldoc in row[5].split(", "):
                        allLeftDocs[ldoc] = ''
                    for rdoc in row[6].split(", "):
                        allRightDocs[rdoc] = ''
                elements['leftType'] = lefttype
                elements['rightType'] = righttype
                elements['leftId'] = leftroot
                elements['rightId'] = rightroot
                elements['leftRoot'] = leftroot
                elements['rightRoot'] = rightroot
                elements['leftDocuments'] = len(allLeftDocs.keys())
                elements['leftDiversity'] = len(allLeftIds.keys())
                elements['rightDocuments'] = len(allRightDocs.keys())
                elements['rightDiversity'] = len(allRightIds.keys())
                elements['joinDiversity'] = len(allJoinIds.keys())
                results.append(elements)

        cursor.close()
        return results

# List of scientific name or class (taxon, habitat, phenotype, use)
def list_terms(app, conn, table):
    query = 'SELECT name, path FROM ' + table + ' ORDER BY name ASC'
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            tmp = {}
            tmp["name"] = row[0]
            tmp["path"] = row[1].split("/")[-1]
            results.append(tmp)
        cursor.close()
        return results

# Path of taxon, habitat, phenotype or use
def path_term(app, conn, name, table):
    query = "SELECT path FROM " + table + " WHERE name = '" + name + "'"
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = cursor.fetchall()
        path = ""
        if len(results) != 0:
            path = results[0]
        cursor.close()
        return path

# List class (habitat, phenotype, use)
def list_obt_class(app, conn, table):
    query = "SELECT name, path FROM " + table
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            tmp = {}
            tmp["name"] = row[0]
            # tmp["path"] = row[1]
            tmp["path"] = row[1].split("/")[-1]
            results.append(tmp)
        cursor.close()
        return results

# List scientific name (taxon)
def list_taxon_name(app, conn):
    query = 'SELECT name, path FROM list_taxon INTERSECT \
SELECT name, path FROM list_taxon_phenotype INTERSECT \
SELECT name, path FROM list_taxon_use ORDER BY "name" ASC'
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            tmp = {}
            tmp["name"] = row[0]
            tmp["path"] = row[1]
            results.append(tmp)
        cursor.close()
        return results

# List taxID
def list_taxid(app, conn, path, qps):
    id = path.split('/')[-1]
    query = "SELECT taxonid FROM taxon t \
             WHERE (t.path like '%/"+id+"/%' OR t.taxonid = '"+id+"') \
             AND t.qps = '"+qps+"'"
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            results.append(row[0])
        cursor.close()
        return results

# List of relations
def list_relations(app, conn, source, taxonid, qps, ontobiotopeid, types):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        query = "SELECT DISTINCT r.form_taxon, r.form_element, r.id_source, \
    r.type, r.source, e.identifier, t.taxonid, t.qps, e.path, t.path, t.diffusion "
        query += " FROM relation r, taxon t, element e"
        query += " WHERE r.id_element = e.id AND t.id = r.id_taxon"
        if source != None and source != []:
            query += " AND ("
            for s in source:
                ss = check_source(s)
                query += "r.source = '" + ss + "' OR "
            query += ")"
            query = re.sub(" OR \)", ")", query)
        if qps in ("yes", "true"):
            query += " AND t.qps = 'yes'"
        if types != None and types != []:
            query += " AND ("
            for type in types:
                query += "r.type = '" + type + "' OR "
            query += ")"
            query = re.sub(' OR \)', ')', query)
        if taxonid != None and taxonid != '':
            query += " AND r.id_taxon in (SELECT id FROM taxon t WHERE t.path like '%/" + taxonid + "/%' OR t.taxonid = '" + taxonid + "')"
        if ontobiotopeid != None and ontobiotopeid != '':
            query += " AND r.id_element in (SELECT id FROM element e WHERE e.path like '%/" + ontobiotopeid + "/%' OR e.identifier = '" + ontobiotopeid + "')"
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        results = []
        for row in cursor.fetchall():
            elements = []
            # docs
            elements.append(row[2])
            # taxon_forms
            str_taxforms = clean_form(row[0])
            elements.append(str_taxforms)
            # relation type
            if row[3] == 'habitat':
                if taxonid != None:
                    elements.append('Lives in')
                else:
                    elements.append('Contains')
            if row[3] == 'phenotype':
                if taxonid != None:
                    elements.append('Exhibits')
                else:
                    elements.append('Is exhibited by')
            if row[3] == 'use':
                if taxonid != None:
                    elements.append('Studied for')
                else:
                    elements.append('Involves')
            # obt_forms
            str_obtforms = clean_form(row[1])
            if row[4] in ('DSMZ', 'GenBank'):
                elements.append(str_obtforms.replace("|", ", "))
            else :
                elements.append(str_obtforms)
            # elements.append(str_obtforms)
            # qps
            elements.append(row[7])
            # Source
            elements.append(row[4])
            elements.append(row[8])
            elements.append(row[9])
            elements.append(row[5])
            elements.append(row[6])
            elements.append(row[10])
            elements.append("")
            elements.append("")
            results.append(elements)
        cursor.close()
        return results

# List of relations (advanced)
def list_advanced_relations(app, conn, source, taxonids, qps, ontobiotopeids, types):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []

        if taxonids == None and ontobiotopeids != None:
            ontobiotopeids = ontobiotopeids.split(", ")
            taxonid = None
            for ontobiotopeid in ontobiotopeids:
                results += create_sql_advanced_relation(source, qps, types, taxonid, ontobiotopeid, cursor)
        elif taxonids != None and ontobiotopeids == None:
            taxonids = taxonids.split(", ")
            ontobiotopeid = None
            for taxonid in taxonids:
                results += create_sql_advanced_relation(source, qps, types, taxonid, ontobiotopeid, cursor)
        else:
            ontobiotopeids = ontobiotopeids.split(", ")
            taxonids = taxonids.split(", ")
            for taxonid in taxonids:
                for ontobiotopeid in ontobiotopeids:
                    results += create_sql_advanced_relation(source, qps, types, taxonid, ontobiotopeid, cursor)

        cursor.close()
        return results
def list_advanced_relations_2(app, conn, source, taxonids, qps, ontobiotopeids, types):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []

        ontobiotopeids = ontobiotopeids.split(", ")
        taxonids = taxonids.split(", ")

        for taxonid in taxonids:
            results += create_sql_advanced_relation(source, qps, None, taxonid, None, cursor)

        for ontobiotopeid in ontobiotopeids:
            results += create_sql_advanced_relation(source, qps, types, None, ontobiotopeid, cursor)

        cursor.close()
        return results
def list_advanced_relations_3(app, conn, source, taxonids, qps, ontobiotopeids, types):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []

        if taxonids != None:
            taxonids = taxonids.split(", ")
        else:
            taxonids = []
        if ontobiotopeids != None:
            ontobiotopeids = ontobiotopeids.split(", ")
        else:
            ontobiotopeids = []

        if taxonids != [] and ontobiotopeids == []:
            query = ""
            for taxonid in taxonids:
                query +=  "SELECT DISTINCT e.identifier FROM element e, relation r, taxon t "
                query += "WHERE e.id = r.id_element AND t.id = r.id_taxon "
                query += "AND r.id_taxon in (SELECT id FROM taxon WHERE "
                if qps == 'yes':
                    query += "qps = '" + qps + "' AND "
                query += "taxonid = '"+taxonid+"' OR path like '%/"+taxonid+"/%') INTERSECT "
            query = re.sub(r" INTERSECT $", "", query)
            try:
                cursor.execute(query)
            except Exception as err:
                print_psycopg2_exception(err)
            ontobiotopeids = []
            for row in cursor.fetchall():
                ontobiotopeids.append(row[0])
            for taxonid in taxonids:
                query = ""
                for ontobiotopeid in ontobiotopeids:
                    query += create_sql_advanced_relation_3(source, qps, types, taxonid, ontobiotopeid)
                query = re.sub(r" UNION $", "", query)
                results += get_result_advanced_relation_3(query, cursor)

        elif taxonids == [] and ontobiotopeids != []:
            query = ""
            for ontobiotopeid in ontobiotopeids:
                query +=  "SELECT DISTINCT t.taxonid FROM element e, relation r, taxon t "
                query += "WHERE e.id = r.id_element AND t.id = r.id_taxon "
                query += "AND r.id_element in (SELECT id FROM element WHERE identifier = '"+ontobiotopeid+"' OR path like '%/"+ontobiotopeid+"/%') INTERSECT "
            query = re.sub(r" INTERSECT $", "", query)
            try:
                cursor.execute(query)
            except Exception as err:
                print_psycopg2_exception(err)
            taxonids = []
            for row in cursor.fetchall():
                taxonids.append(row[0])
            for ontobiotopeid in ontobiotopeids:
                query = ""
                for taxonid in taxonids:
                    query += create_sql_advanced_relation_3(source, qps, types, taxonid, ontobiotopeid)
                query = re.sub(r" UNION $", "", query)
                results += get_result_advanced_relation_3(query, cursor)
        else:
            if len(taxonids) > 1 and len(ontobiotopeids) == 1:
                query = ""
                for taxonid in taxonids:
                    query +=  "SELECT DISTINCT e.identifier FROM element e, relation r, taxon t "
                    query += "WHERE e.id = r.id_element AND t.id = r.id_taxon "
                    query += "AND r.id_taxon in (SELECT id FROM taxon WHERE "
                    if qps == 'yes':
                        query += "qps = '" + qps + "' AND "
                    query += "taxonid = '"+taxonid+"' OR path like '%/"+taxonid+"/%') "
                    query += "AND r.id_element in (SELECT id FROM element WHERE identifier = '"+ontobiotopeids[0]+"' OR path like '%/"+ontobiotopeids[0]+"/%') INTERSECT "
                query = re.sub(r" INTERSECT $", "", query)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                ontobiotopeids = []
                for row in cursor.fetchall():
                    ontobiotopeids.append(row[0])
                if ontobiotopeids != []:
                    for taxonid in taxonids:
                        query = ""
                        for ontobiotopeid in ontobiotopeids:
                            query += create_sql_advanced_relation_3(source, qps, types, taxonid, ontobiotopeid)
                        query = re.sub(r" UNION $", "", query)
                        results += get_result_advanced_relation_3(query, cursor)
                else:
                    return []

            else:
                query = ""
                for ontobiotopeid in ontobiotopeids:
                    query +=  "SELECT DISTINCT t.taxonid FROM element e, relation r, taxon t "
                    query += "WHERE e.id = r.id_element AND t.id = r.id_taxon "
                    query += "AND r.id_taxon in (SELECT id FROM taxon WHERE "
                    if qps == 'yes':
                        query += "qps = '" + qps + "' AND "
                    query += "taxonid = '"+taxonids[0]+"' OR path like '%/"+taxonids[0]+"/%') "
                    query += "AND r.id_element in (SELECT id FROM element WHERE identifier = '"+ontobiotopeid+"' OR path like '%/"+ontobiotopeid+"/%') INTERSECT "
                query = re.sub(r" INTERSECT $", "", query)
                try:
                    cursor.execute(query)
                except Exception as err:
                    print_psycopg2_exception(err)
                taxonids = []
                for row in cursor.fetchall():
                    taxonids.append(row[0])
                if taxonids != []:
                    for ontobiotopeid in ontobiotopeids:
                        query = ""
                        for taxonid in taxonids:
                            query += create_sql_advanced_relation_3(source, qps, types, taxonid, ontobiotopeid)
                        query = re.sub(r" UNION $", "", query)
                        results += get_result_advanced_relation_3(query, cursor)
                else:
                    return []

        cursor.close()
        return results
def list_advanced_relations_4(app, conn, query, source, taxonid, obtid):
    conn = get_db(app)
    if conn != None:
        cursor = conn.cursor()
        results = []
        try:
            cursor.execute(query)
        except Exception as err:
            print_psycopg2_exception(err)
        if obtid != None:
            ontobiotopeids = obtid.split(", ")
        else:
            ontobiotopeids = []
        taxonids = []
        for row in cursor.fetchall():
            taxonids.append(row[0])
        if taxonids == []:
            results = []
        else:
            for taxonid in taxonids:
                query = ""
                for ontobiotopeid in ontobiotopeids:
                    query += create_sql_advanced_relation_3(source, '', '', taxonid, ontobiotopeid)
                query = re.sub(r" UNION $", "", query)
                results += get_result_advanced_relation_3(query, cursor)

        cursor.close()
        return results
def create_sql_advanced_relation(source, qps, types, taxonid, ontobiotopeid, cursor):
    results = []
    query = "SELECT DISTINCT r.form_taxon, r.form_element, r.id_source, \
r.type, r.source, e.identifier, t.taxonid, t.qps, e.path, t.path, t.diffusion"
    query += " FROM relation r, taxon t, element e"
    query += " WHERE r.id_element = e.id AND t.id = r.id_taxon"
    if source != None:
        source = re.sub(", ", "', '", source)
        query += " AND r.source in ('" + source + "') "
    if qps == 'yes':
        query += " AND t.qps = 'yes'"
    if taxonid != None:
        query += " AND r.id_taxon in (SELECT id FROM taxon WHERE taxonid = '"+taxonid+"' OR path like '%/"+taxonid+"/%')"
    if ontobiotopeid != None:
        query += " AND r.id_element in (SELECT id FROM element WHERE identifier = '"+ontobiotopeid+"' OR path like '%/"+ontobiotopeid+"/%')"
    try:
        cursor.execute(query)
    except Exception as err:
        print_psycopg2_exception(err)
    for row in cursor.fetchall():
        elements = []
        elements.append(row[2])
        # taxon_forms
        str_taxforms = clean_form(row[0])
        elements.append(str_taxforms)
        # relation type
        if row[3] == 'habitat':
            elements.append('Lives in')
        if row[3] == 'phenotype':
            elements.append('Exhibits')
        if row[3] == 'use':
            elements.append('Studied for')
        # obt_forms
        str_obtforms = clean_form(row[1])
        if row[4] in ('DSMZ', 'GenBank'):
            elements.append(str_obtforms.replace("|", ", "))
        else :
            elements.append(str_obtforms)
        # elements.append(str_obtforms)
        # qps
        elements.append(row[7])
        # Source
        elements.append(row[4])
        elements.append(row[8])
        elements.append(row[9])
        elements.append(row[5])
        elements.append(row[6])
        elements.append(row[10])
        elements.append("")
        elements.append("")
        results.append(elements)
    return results
def create_sql_advanced_relation_3(source, qps, types, taxonid, ontobiotopeid):
    query = "SELECT DISTINCT r.form_taxon, r.form_element, r.id_source,"
    query += " r.type, r.source, e.identifier, t.taxonid, t.qps, e.path, t.path, t.diffusion"
    query += " AS test"
    query += " FROM relation r, taxon t, element e"
    query += " WHERE r.id_element = e.id AND t.id = r.id_taxon"
    if source != None:
        source = re.sub(", ", "', '", source)
        query += " AND r.source in ('" + source + "') "
    if qps == 'yes':
        query += " AND t.qps = 'yes'"
    if ontobiotopeid != None:
        query += " AND r.id_element in (SELECT id FROM element WHERE identifier = '"+ontobiotopeid+"' OR path like '%/"+ontobiotopeid+"/%')"
    if taxonid != None:
        query += " AND r.id_taxon in (SELECT id FROM taxon WHERE taxonid = '"+taxonid+"')"
    query += " UNION "
    return query
def get_result_advanced_relation_3(query, cursor):
    try:
        cursor.execute(query)
    except Exception as err:
        print_psycopg2_exception(err)
    results = []
    for row in cursor.fetchall():
        elements = []
        elements.append(row[2])
        # taxon_forms
        str_taxforms = clean_form(row[0])
        elements.append(str_taxforms)
        # relation type
        if row[3] == 'habitat':
            elements.append('Lives in')
        if row[3] == 'phenotype':
            elements.append('Exhibits')
        if row[3] == 'use':
            elements.append('Studied for')
        # obt_forms
        str_obtforms = clean_form(row[1])
        if row[4] in ('DSMZ', 'GenBank'):
            elements.append(str_obtforms.replace("|", ", "))
        else :
            elements.append(str_obtforms)
        # elements.append(str_obtforms)
        # qps
        elements.append(row[7])
        # Source
        elements.append(row[4])
        elements.append(row[8])
        elements.append(row[9])
        elements.append(row[5])
        elements.append(row[6])
        elements.append(row[10])
        elements.append("")
        elements.append("")
        results.append(elements)
    return results

# Utils
def add_sources(query, source):
    for col in ['lr', 'rr']:
        query += " AND ("
        for s in source:
            ss = check_source(s)
            query += col+".source = '" + ss + "' OR "
        query += ")"
        query = re.sub(" OR \)", ")", query)
    return query
def clean_form(form):
    form = re.sub("<em>", "", form)
    form = re.sub("</em> ", ", ", form)
    return form
def cut_obt_path(lpath):
    sql_path = ""
    for p in lpath:
        sql_path += "OR e.path like '" + p + "/%' "
    sql_path = re.sub('^OR ', '', sql_path)
    return sql_path
