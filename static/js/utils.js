/**
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
**/

export function format_docs(relation, alvisir, rtype) {

  let docs = "";
  if ( relation[0].includes(', ') ) { docs = relation[0].split(', '); }
  else                              { docs = relation[0].split(','); }
  let docids = "";
  for ( let i = 0; i < docs.length ; i++ ) {
    if ( relation[5] == 'PubMed') {
      let taxon = relation[1].split(', ')[0].replace(/\s/g, "+").replace("=", "\\=").replace("~","\\~").replace("(","\\(").replace(")","\\)").replace("[","\\[").replace("]","\\]").replace(/\+(and|or|not)\+/, "\+\\$1\+");
      let element = relation[3].split(', ')[0].replace(/\s/g, "+").replace("=", "\\=").replace("~","\\~").replace("(","\\(").replace(")","\\)").replace("[","\\[").replace("]","\\]").replace(/\+(and|or|not)\+/, "\+\\$1\+");
      let path = relation[6].split(',')[0];
      let rel_type = '';
      if ( rtype == 'habitat' )         { rel_type = 'lives+in'; }
      else if ( rtype == 'phenotype' )  { rel_type = 'exhibits'; }
      else if ( rtype == 'use' )        { rel_type = 'studied+for'; }
      docids += "<a target='_blank' class='doc' href='"+alvisir+"search?q=%22"+taxon+"%22+"+rel_type+"+%7B"+rtype+"%7D"+path+"/+%22"+element+"%22+pmid="+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'GenBank' ) {
      docids += "<a target='_blank' class='doc' href='https://www.ncbi.nlm.nih.gov/nuccore/"+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'DSMZ' ) {
      docids += "<a target='_blank' class='doc' href='https://bacdive.dsmz.de/strain/"+docs[i]+"'>"+docs[i]+"</a>, ";
    }
    else if ( relation[5] == 'CIRM-BIA' && relation[10] != 'Restricted' ) {
      let cirmid = docs[i].split(', ')[0].replace(/.*\sCIRM-BIA\s/g, "");
      if ( cirmid != '-' ) {
        docids += "<a target='_blank' class='doc' href='https://collection-cirmbia.fr/page/Display_souches/"+cirmid+"'>"+cirmid+"</a>, ";
      }
    }
    // CIRM-Levures
    // CIRM-CBPF
  }
  docids = docids.slice(0, -2);

  return docids;
}
